import React from 'react';
import {Image, ScrollView, StyleSheet, View} from 'react-native';
import Perangkat from './assets/kotak.png';
import LogoYoutube from './assets/youtube.png';
import Lonceng from './assets/lonceng.png';
import Cari from './assets/cari.png';
import GambarAkun from './assets/akun.jpg';
import Video from './component/video';

const App = () => {
  return(
    <View>
      <View style={{ backgroundColor: 'white', flexDirection: 'row', height: 50}}>
        <Image source={LogoYoutube} style={setting.LogoYoutube} />
        <Image source={Perangkat} style={setting.Perangkat} />
        <Image source={Lonceng} style={setting.Lonceng} />
        <Image source={Cari} style={setting.Cari} />
        <Image source={GambarAkun} style={setting.GambarAkun} />
      </View>
      <ScrollView>
        <Video />
        <Video />
        <Video />
        <Video />
        <Video />
        <Video />
      </ScrollView>
    </View>
  );
};

const setting = StyleSheet.create({
  LogoYoutube: {
    width: 93,
    height: 20.7,
    marginLeft: 14,
    marginTop: 13,
  },

  Perangkat: {
    width: 22,
    height: 30,
    marginTop : 9.5,
    marginLeft: 92,
  },

  Lonceng: {
    width: 20,
    height: 22,
    marginTop : 13,
    marginLeft: 20,
    
  },

  Cari: {
    width: 20,
    height: 28,
    marginTop : 10,
    marginLeft: 18,
  },

  GambarAkun: {
    width: 30,
    height: 30,
    marginTop : 10,
    marginLeft: 16,
    borderRadius: 20,
  },
});

export default App;