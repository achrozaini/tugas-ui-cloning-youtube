import React from 'react';
import {Image, StyleSheet, Text, View} from 'react-native';
import img from '../assets/bakso.jpg';
import GambarAkun from '../assets/akun.jpg';

const Video = () => {
    return (
        <View style={setting.box}>
            <Image source={img} style={setting.img} />
            <View style={setting.judul}>
                <Image source={GambarAkun} style={setting.GambarAkun} />
                <View style={setting.jud}>
                    <Text style={setting.teks}>Tutorial Makan Bakso</Text>
                    <Text style={setting.teks2}>Ach. Rozaini TV  20jt x ditonton  1 jam yang lalu</Text>
                </View>
            </View>
        </View>
    );
};

const setting = StyleSheet.create({
    box: {
        backgroundColor: 'white',
    },

    img: {
        width: 360,
        height: 200,
    },

    judul: {
        flexDirection: 'row',
    },

    jud: {
        flexDirection: 'column',
    },

    GambarAkun: {
        width: 35,
        height: 35,
        marginTop : 13,
        marginLeft: 16,
        borderRadius: 20,
    },

    teks: {
        fontSize: 18,
        fontWeight: 'bold',
        marginLeft: 15,
        marginTop: 8,
    },
    teks2: {
        fontSize: 12,
        marginLeft: 15,
        marginTop: 2,
        paddingBottom: 20,
    },
});

export default Video;